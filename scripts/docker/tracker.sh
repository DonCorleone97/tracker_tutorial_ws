#!/usr/bin/env bash
export WORKSPACE_PATH="/home/$USER/tracker_ws"

cd $WORKSPACE_PATH

docker build -t tutorial-tracker:latest -f dockerfiles/tracker/Dockerfile .
