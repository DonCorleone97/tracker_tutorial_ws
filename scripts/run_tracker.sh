#!/bin/bash

docker stop tracker-dev
docker rm tracker-dev
docker run \
    -it \
    --privileged \
    --name tracker-dev \
    --net host \
    --mount type=bind,source="$(pwd)",target=/ws/ \
    andysaba/tutorial-tracker:latest /bin/bash
