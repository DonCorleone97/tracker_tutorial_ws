#!/bin/bash

git lfs install

cd ~/tracker_tutorial_ws

git submodule update --init --recursive
git lfs pull
